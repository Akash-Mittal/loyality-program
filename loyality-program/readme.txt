
To Do : Write Junits with code coverage of 90% excluding Domains and Util Sonar Coverage.

Time Taken : 2 Days.

Estimated Hours : 48.

Actual Hours:30


Assumptions.

�  The program will run through bat file - for unix/linux shell script need to be written
�  Up until Now ABC limited had no loyalty program.
�  They are Planning to Implement this for Existing (All transactions)and New Customers on (Per Transaction Basis).  
�  By Default the Customer Class will be Normal.
�  Writing the Junits for Utils Classes is not in the Scope of this problem.
�  The Junit Code Coverage :Target 90%
�  No Front END - it would run like a cron JOB.
�  All the Customer Report will be generated in Single XML in a single page and after words it can be conveniently morphed to HTML/PDF/JSON.
�  Customer Not Enrolled has card number < 0
�  As it it�s an EOD activity - all the data will come in XML file.
�  To keep it simple - Concurrency and Serialization and IOC has not been taken into account.
�  Code will be delivered up to Service layer.
�  Validations : Low level 
	For Hogh level validations - All the non validations was initially supposed to be hibernate , howevere later it was found that
	all the XML parsing was done by Xstream in that case springs validator need to be auto injected
	which is out of the scope of this assignment.
�  Application has been tested for happy cases only. Regression Testsing and Junits is out of scope.
 

 
Features:

�  Completely Dynamic - Supports loading properties at runtime.
�  XStream Frame work for Easy XML reading and Writing.
�  Integrated with POM file - simple execute the mvn bat file to test the application.
�  Junit 4 and Mockito for Test Cases.
�  Log4 J Based Logging.
�  Can be Scheduled as a cron job.


How to Run and Build the Applications.

To build the applications use.
runmaven.bat
To Run the applications use.
run-loyality-program.bat
The application will need the complete path to config.properties , evaluater need to change it accordingly.
config.properties.
	# This is the default file having current data of ABC
	com.abc.default.xml.filename=D:\\Java\\Practise\\ABC-Limited\\loyality-program\\customerDefaultData.xml
	# This is the EOD feed file
	com.abc.eod.transaction.filename=D:\\Java\\Practise\\ABC-Limited\\loyality-program\\eodTransactionsData.xml
	# This is the generated report  after words it can be conveniently morphed to HTML/PDF/JSON.
	com.abc.customer.database.xml.filename=D:\\Java\\Practise\\ABC-Limited\\loyality-program\\customerDatabase.xml
	
	