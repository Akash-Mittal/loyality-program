/*
 * 
 */
package com.mettl.abc.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import org.apache.log4j.Logger;
import com.mettl.abc.domains.Customer;
import com.mettl.abc.domains.EODTransactions;
import com.mettl.abc.domains.Transaction;
import com.mettl.abc.utils.FileUtils;
import com.mettl.abc.utils.XMLUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomerLoyalityServiceImpl.
 *
 * @author Akash Mittal
 * @@task Loyality Program Assignment
 */
public class CustomerLoyalityServiceImpl implements CustomerLoyalityService {
	
	/** The logger. */
	private static Logger logger = Logger
			.getLogger(CustomerLoyalityServiceImpl.class);
	
	/** The properties. */
	private Properties properties;
	
	/**
	 * Instantiates a new customer loyality service impl.
	 *
	 * @param properties the properties
	 */
	public CustomerLoyalityServiceImpl(Properties properties) {
		this.properties=properties;
	}

	
	
	/* (non-Javadoc)
	 * @see com.mettl.abc.service.CustomerLoyalityService#getExistingCustomer()
	 * This Method emulated the existing customers from the database.
	 *  */
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Customer> getExistingCustomer() throws IOException {
	ArrayList<Customer> customersList =(ArrayList<Customer>) XMLUtil
				.loadXML(FileUtils.loadFile(this.properties.getProperty("com.abc.default.xml.filename")));
	for (int i=0;i<customersList.size();i++) {
		 List<Transaction> transactionList=customersList.get(i).getCustomerTransactions();
		 for (Transaction transaction : transactionList) {
			 customersList.set(i,
			 computeBusinessClass(customersList.get(i), transaction));
		 }
	}
	return customersList;
	}




	/* (non-Javadoc)
	 * @see com.mettl.abc.service.CustomerLoyalityService#getEODTransactionsData()
	 	This method loads the EOD transactions feed from xml file
	 	Low level validations are applied - high level validations are out of scope
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<EODTransactions> getEODTransactionsData() throws IOException {
	logger.debug("Loaded Property "+this.properties.getProperty("com.abc.eod.transaction.filename"));	
		return (ArrayList<EODTransactions>) XMLUtil
				.loadXML(FileUtils.loadFile(this.properties.getProperty("com.abc.eod.transaction.filename")));

	}

	
	
	
	/* (non-Javadoc)
	 * @see com.mettl.abc.service.CustomerLoyalityService#addTransactionDetailsToCustomer(java.util.ArrayList, java.util.ArrayList)
	 This function will First Synch the Customer Transaction Data with Internal Database
	 customerDefaultData.xml list the Database befor dataprocessing
	customerDatabase.xml will be new state of the DB after the EOD data processing
	 */
	public ArrayList<Customer> addTransactionDetailsToCustomer(
			ArrayList<Customer> existingCustomersList,
			ArrayList<EODTransactions> eODTransactionsList) throws IOException {
		if (Objects.nonNull(eODTransactionsList)
				&& Objects.nonNull(existingCustomersList)) {
			Collections.sort(existingCustomersList);
			// printMe(existingCustomersList);

			for (EODTransactions eodTransaction : eODTransactionsList) {
				if (Objects.nonNull(eodTransaction
						.getCustomerLoyalityCardNumber())) {
					Customer customerToSearch = new Customer();
					customerToSearch
							.setCustomerLoyalityCardNumber(eodTransaction
									.getCustomerLoyalityCardNumber());
					int customIndexLocation = Collections.binarySearch(
							existingCustomersList, customerToSearch);
				
					// Apply Rules for Class
					
					if (customIndexLocation > -1) {
						logger.debug("Point 2,3,4 For Gold Silve and Normal");
						logger.debug("Customer Found Customer Name"
								+ existingCustomersList
										.get(customIndexLocation).toString());
						logger.debug("Adding transaction to the Existing Customer"
								+ eodTransaction.toString());

						existingCustomersList
								.get(customIndexLocation)
								.getCustomerTransactions()
								.add(parseEODTransactiontoTransaction(eodTransaction));
						
						// This Will add the Customer Class and Reward  Points
						existingCustomersList.set(customIndexLocation,
						computeBusinessClass(existingCustomersList.get(customIndexLocation),parseEODTransactiontoTransaction(eodTransaction)));
						
						if (Objects.nonNull(eodTransaction.getCustomerName())) {
							if(!eodTransaction.getCustomerName().isEmpty()){
							if (!(existingCustomersList
									.get(customIndexLocation).getCustomerName()
									.equalsIgnoreCase(eodTransaction
											.getCustomerName()))) {
								logger.debug("Updating Customer Names + From "
										+ existingCustomersList.get(
												customIndexLocation)
												.getCustomerName() + " to "
										+ eodTransaction.getCustomerName());
								existingCustomersList.get(customIndexLocation)
										.setCustomerName(
												eodTransaction
														.getCustomerName());
							}
						}
						}
						
						// Check for email changes on each transaction
						if (Objects.nonNull(eodTransaction.getEmail())) {
							if(!eodTransaction.getEmail().isEmpty()){
							if (!(existingCustomersList
									.get(customIndexLocation).getEmail()
									.equalsIgnoreCase(eodTransaction.getEmail()))) {
								logger.debug("Updating Customer Email + From "
										+ existingCustomersList.get(
												customIndexLocation).getEmail()
										+ " to " + eodTransaction.getEmail());

								existingCustomersList.get(customIndexLocation)
										.setEmail(eodTransaction.getEmail());
							}
							}
						}

					} else if (customIndexLocation < 0) {
						if (Objects.nonNull(eodTransaction
								.getCustomerLoyalityCardNumber())) {
							if(eodTransaction.getCustomerLoyalityCardNumber()>0){
							// This is the Case for New customer with LCN
							logger.debug("1. New Customer that enrolls will get 100 Points as joining bonus.");
							logger.debug("Adding New Customer"
									+ eodTransaction.toString());
							existingCustomersList
									.add(parseEODTransactiontoToCustomer(eodTransaction));
							// After Adding a new Customer the ordering needs to
							// be done
							// Would not be needed if persistamce was there
							logger.debug("Added New Customer - Sorting the Collection Again");
							existingCustomersList.set(existingCustomersList.size()-1,
									computeBusinessClass(existingCustomersList.get(existingCustomersList.size()-1),parseEODTransactiontoTransaction(eodTransaction)));
							Collections.sort(existingCustomersList);
							// This Will add the Customer Class and Reward  Points
							
						}
						} else {
							// This is the Case for without LCN // Not Enrolled
							logger.debug("5. Customers who have not enrolled will not get any point.");

						}
					}

				}

			}
		} else {
			return null;
		}

		return existingCustomersList;
	}

	/**
	 * Prints the me.
	 *
	 * @param arrayList the array list
	 */
	public static void printMe(ArrayList<Customer> arrayList) {
		for (Customer customer : arrayList) {
			logger.debug(customer.toString());
		}

	}

	/**
	 * Parses the eod transactionto transaction.
	 *
	 * @param eodTransaction the eod transaction
	 * @return the transaction
	 */
	private Transaction parseEODTransactiontoTransaction(
			EODTransactions eodTransaction) {
		Transaction transaction = new Transaction();
		if (Objects.nonNull(eodTransaction)) {
			if (Objects.nonNull(eodTransaction.getPurchaseAmount()))
				transaction.setPurchaseAmount(eodTransaction
						.getPurchaseAmount());
			if (Objects.nonNull(eodTransaction.getPurchaseDate()))
				transaction.setPurchaseDate(eodTransaction.getPurchaseDate());
			if (Objects.nonNull(eodTransaction.getTransactionID()))
				transaction.setTransactionID(eodTransaction.getTransactionID());
			return transaction;
		}
		return null;
	}

	/**
	 * Parses the eod transactionto to customer.
	 *
	 * @param eodTransaction the eod transaction
	 * @return the customer
	 */
	private Customer parseEODTransactiontoToCustomer(
			EODTransactions eodTransaction) {
		Customer customer = new Customer();
		customer.setCustomerID(((new java.util.Random().nextLong())%99)+9999);
		logger.debug("Customer ID generated +"+ customer.getCustomerID());
		customer.getCustomerTransactions().add(
				parseEODTransactiontoTransaction(eodTransaction));
		customer.setCustomerClass(CustomerLoyalityService.CustomerClass.Normal
				.toString());
		customer.setRewardPoints(0L);
		if (Objects.nonNull(eodTransaction)) {
			if (Objects.nonNull(eodTransaction.getCustomerName()))
				customer.setCustomerName(eodTransaction.getCustomerName());
			if (Objects.nonNull(eodTransaction.getEmail()))
				customer.setEmail(eodTransaction.getEmail());
			if (Objects.nonNull(eodTransaction.getCustomerLoyalityCardNumber()))
				customer.setCustomerLoyalityCardNumber(eodTransaction
						.getCustomerLoyalityCardNumber());
			return customer;
		}
		return null;
	}

	/**
	 * Generate reward points.
	 *
	 * @param customerClass the customer class
	 * @param transactionAmount the transaction amount
	 * @return the long
	 */
	private static Long generateRewardPoints(String customerClass,Double transactionAmount){
		if(CustomerLoyalityService.CustomerClass.Gold.toString().equals(customerClass))
			return (transactionAmount.longValue()/500)*25;
		if(CustomerLoyalityService.CustomerClass.Silver.toString().equals(customerClass))
			return (transactionAmount.longValue()/100)*2;
		if(CustomerLoyalityService.CustomerClass.Normal.toString().equals(customerClass))
			return (transactionAmount.longValue()/100);
			
		return null;
	}
	
	/**
	 * Gets the total purchase amount.
	 *
	 * @param transactionList the transaction list
	 * @return the total purchase amount
	 */
	private static Double gettotalPurchaseAmount(List<Transaction> transactionList){
		Double totalPurchaseAmount =0.0;
		for (Transaction transaction : transactionList) {
			totalPurchaseAmount = totalPurchaseAmount + transaction.getPurchaseAmount();
		}
		return totalPurchaseAmount;
	}
	
	/**
	 * Compute business class.
	 *
	 * @param customer the customer
	 * @param transaction the transaction
	 * @return the customer
	 */
	private static Customer computeBusinessClass(Customer customer,Transaction transaction){
		Double totalPurchaseAmount =0.0;
		totalPurchaseAmount = gettotalPurchaseAmount(customer.getCustomerTransactions());
		Long existingRewardPoints = customer.getRewardPoints();
		logger.debug("Before Setting the Customer Class and Reward Points "+customer);
		
		if(totalPurchaseAmount > 50000){
			customer.setCustomerClass(CustomerLoyalityService.CustomerClass.Gold.toString());
		}	
		if(totalPurchaseAmount > 25000 && totalPurchaseAmount <=50000){
			customer.setCustomerClass(CustomerLoyalityService.CustomerClass.Silver.toString());
		}
		if(totalPurchaseAmount <=25000){
			customer.setCustomerClass(CustomerLoyalityService.CustomerClass.Normal.toString());
		}
		existingRewardPoints = existingRewardPoints + generateRewardPoints(customer.getCustomerClass(),transaction.getPurchaseAmount());
		customer.setRewardPoints(existingRewardPoints);
		logger.debug("After Setting the Customer Class and Reward Points "+customer);
		return customer;
	}
	

	/* (non-Javadoc)
	 * @see com.mettl.abc.service.CustomerLoyalityService#printReportForEachCustomer(java.util.ArrayList, java.lang.String)
	 */
	@Override
	public Boolean printReportForEachCustomer(
		ArrayList<Customer> existingCustomers,String format) throws IOException {
		if(format.equalsIgnoreCase("XML")){
		FileUtils.writeFile(properties.getProperty("com.abc.customer.database.xml.filename"),XMLUtil.saveXML(existingCustomers));
		logger.debug("XML report Succesfully created on "+properties.getProperty("com.abc.customer.database.xml.filename"));
		return true;
		}
		return false;
	}


	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws IOException {
		CustomerLoyalityServiceImpl customerLoyalityServiceImpl = new CustomerLoyalityServiceImpl(FileUtils.loadProperties("/config.properties"));
	List<EODTransactions> aa = customerLoyalityServiceImpl.getEODTransactionsData();
		for (EODTransactions eodTransactions : aa) {
			System.out.println(eodTransactions.toString());
		}
	
	}

}
