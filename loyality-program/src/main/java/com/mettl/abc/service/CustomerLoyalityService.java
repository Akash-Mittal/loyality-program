/*
 * 
 */
package com.mettl.abc.service;

import java.io.IOException;
import java.util.ArrayList;

import com.mettl.abc.domains.Customer;
import com.mettl.abc.domains.EODTransactions;

// TODO: Auto-generated Javadoc
/**
 * The Interface CustomerLoyalityService.
 *
 * @author Akash Mittal
 * @@task Loyality Program Assignment
 */
public interface CustomerLoyalityService {
	
	/**
	 * The Enum CustomerClass.
	 *
	 * @author Akash Mittal
	 * @@task Loyality Program Assignment
	 */
	enum CustomerClass {
		
		/** The Gold. */
		Gold,
/** The Silver. */
Silver,
/** The Normal. */
Normal;
	}	
	
	
 /**
  * Gets the existing customer.
  *
  * @return the existing customer
  * @throws IOException Signals that an I/O exception has occurred.
  */
 public ArrayList<Customer> getExistingCustomer() throws IOException;
 
 /**
  * Gets the EOD transactions data.
  *
  * @return the EOD transactions data
  * @throws IOException Signals that an I/O exception has occurred.
  */
 public ArrayList<EODTransactions> getEODTransactionsData() throws IOException;
 
 /**
  * Adds the transaction details to customer.
  *
  * @param existingCustomers the existing customers
  * @param eODTransactions the e od transactions
  * @return the array list
  * @throws IOException Signals that an I/O exception has occurred.
  */
 public ArrayList<Customer>  addTransactionDetailsToCustomer(ArrayList<Customer> existingCustomers,ArrayList<EODTransactions> eODTransactions) throws IOException;
 
 
 /**
  * Prints the report for each customer.
  *
  * @param existingCustomers the existing customers
  * @param format the format
  * @return the boolean
  * @throws IOException Signals that an I/O exception has occurred.
  */
 public Boolean printReportForEachCustomer(ArrayList<Customer> existingCustomers,
		String format) throws IOException;
}
