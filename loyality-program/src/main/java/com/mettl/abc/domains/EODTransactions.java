/*
 * 
 */
package com.mettl.abc.domains;


import java.util.Date;

import javax.validation.constraints.NotNull;

// TODO: Auto-generated Javadoc
// This is temprary Model Which will mapped with the 
//incoming XML feed for EOD Data

/**
 * The Class EODTransactions.
 *
 * @author Akash Mittal
 * @@task Loyality Program Assignment
 */
public class EODTransactions {
	
	/** The Customer name. */
	private String CustomerName;
	
	/** The Email. */
	private String Email;
	
	/** The customer loyality card number. */
	private Long customerLoyalityCardNumber;
	
	/** The purchase amount. */
	@NotNull
	private Double purchaseAmount;
	
	/** The purchase date. */
	@NotNull
	private Date purchaseDate;
	
	/** The transaction id. */
	@NotNull
	private Long transactionID;
	
	/**
	 * Gets the customer name.
	 *
	 * @return the customer name
	 */
	public String getCustomerName() {
		return CustomerName;
	}
	
	/**
	 * Sets the customer name.
	 *
	 * @param customerName the new customer name
	 */
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	
	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return Email;
	}
	
	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		Email = email;
	}
	
	/**
	 * Gets the customer loyality card number.
	 *
	 * @return the customer loyality card number
	 */
	public Long getCustomerLoyalityCardNumber() {
		return customerLoyalityCardNumber;
	}
	
	/**
	 * Sets the customer loyality card number.
	 *
	 * @param customerLoyalityCardNumber the new customer loyality card number
	 */
	public void setCustomerLoyalityCardNumber(Long customerLoyalityCardNumber) {
		this.customerLoyalityCardNumber = customerLoyalityCardNumber;
	}
	
	/**
	 * Gets the purchase amount.
	 *
	 * @return the purchase amount
	 */
	public Double getPurchaseAmount() {
		return purchaseAmount;
	}
	
	/**
	 * Sets the purchase amount.
	 *
	 * @param purchaseAmount the new purchase amount
	 */
	public void setPurchaseAmount(Double purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}
	
	/**
	 * Gets the purchase date.
	 *
	 * @return the purchase date
	 */
	public Date getPurchaseDate() {
		return purchaseDate;
	}
	
	/**
	 * Sets the purchase date.
	 *
	 * @param purchaseDate the new purchase date
	 */
	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
	/**
	 * Gets the transaction id.
	 *
	 * @return the transaction id
	 */
	public Long getTransactionID() {
		return transactionID;
	}
	
	/**
	 * Sets the transaction id.
	 *
	 * @param transactionID the new transaction id
	 */
	public void setTransactionID(Long transactionID) {
		this.transactionID = transactionID;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EODTransactions [CustomerName=" + CustomerName + ", Email="
				+ Email + ", customerLoyalityCardNumber="
				+ customerLoyalityCardNumber + ", purchaseAmount="
				+ purchaseAmount + ", purchaseDate=" + purchaseDate
				+ ", transactionID=" + transactionID + "]";
	}

}
