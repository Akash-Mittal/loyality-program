/*
 * 
 */

package com.mettl.abc.domains;


import java.util.ArrayList;
import java.util.List;

import com.mettl.abc.utils.DefaultData;

// TODO: Auto-generated Javadoc
/**
 * The Class Customer.
 *
 * @author Akash Mittal
 * @@task Loyality Program Assignment
 */
public class Customer implements Comparable<Customer>{
	
	/** The customer id. */
	private Long customerID;
	
	/** The customer loyality card number. */
	private Long customerLoyalityCardNumber;
	
	/** The Customer name. */
	private String CustomerName;
	
	/** The Email. */
	private String Email;
	
	/** The customer transactions. */
	private List<Transaction> customerTransactions = new ArrayList<Transaction>();
	
	/** The customer class. */
	private String customerClass;
	
	/** The reward points. */
	private Long rewardPoints;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return this.customerID.hashCode();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
        if(this == obj) {
            return true;
       }
       if (!(obj instanceof Customer)) {
              return false; 
       }
       Customer customer = (Customer)obj;
       if(customer.getCustomerName().equalsIgnoreCase(this.getCustomerName())
    		   &&customer.getCustomerID().equals(this.getCustomerID()))
       return true;
       return false;
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		System.out.println(DefaultData.getCustomerDefaultData());
	}
	
	/**
	 * Gets the customer id.
	 *
	 * @return the customer id
	 */
	public Long getCustomerID() {
		return customerID;
	}
	
	/**
	 * Sets the customer id.
	 *
	 * @param customerID the new customer id
	 */
	public void setCustomerID(Long customerID) {
		this.customerID = customerID;
	}
	
	/**
	 * Gets the customer loyality card number.
	 *
	 * @return the customer loyality card number
	 */
	public Long getCustomerLoyalityCardNumber() {
		return customerLoyalityCardNumber;
	}
	
	/**
	 * Sets the customer loyality card number.
	 *
	 * @param customerLoyalityCardNumber the new customer loyality card number
	 */
	public void setCustomerLoyalityCardNumber(Long customerLoyalityCardNumber) {
		this.customerLoyalityCardNumber = customerLoyalityCardNumber;
	}
	
	/**
	 * Gets the customer name.
	 *
	 * @return the customer name
	 */
	public String getCustomerName() {
		return CustomerName;
	}
	
	/**
	 * Sets the customer name.
	 *
	 * @param customerName the new customer name
	 */
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	
	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return Email;
	}
	
	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		Email = email;
	}
	
	/**
	 * Gets the customer transactions.
	 *
	 * @return the customer transactions
	 */
	public List<Transaction> getCustomerTransactions() {
		return customerTransactions;
	}
	
	/**
	 * Sets the customer transactions.
	 *
	 * @param customerTransactions the new customer transactions
	 */
	public void setCustomerTransactions(List<Transaction> customerTransactions) {
		this.customerTransactions = customerTransactions;
	}
	
	/**
	 * Gets the customer class.
	 *
	 * @return the customer class
	 */
	public String getCustomerClass() {
		return customerClass;
	}
	
	/**
	 * Sets the customer class.
	 *
	 * @param customerClass the new customer class
	 */
	public void setCustomerClass(String customerClass) {
		this.customerClass = customerClass;
	}
	
	/**
	 * Gets the reward points.
	 *
	 * @return the reward points
	 */
	public Long getRewardPoints() {
		return rewardPoints;
	}
	
	/**
	 * Sets the reward points.
	 *
	 * @param rewardPoints the new reward points
	 */
	public void setRewardPoints(Long rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Customer [customerID=" + customerID
				+ ", customerLoyalityCardNumber=" + customerLoyalityCardNumber
				+ ", CustomerName=" + CustomerName + ", Email=" + Email
				+ ", customerTransactions=" + customerTransactions
				+ ", customerClass=" + customerClass + ", rewardPoints="
				+ rewardPoints + "]";
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Customer o) {
		return this.getCustomerLoyalityCardNumber().intValue()-o.getCustomerLoyalityCardNumber().intValue();
	}
	
	/**
	 * Gets the cusktomer id.
	 *
	 * @return the cusktomer id
	 */
	public Long getCusktomerID() {
		return customerID;
	}
	
	/**
	 * Sets the cusktomer id.
	 *
	 * @param cusktomerID the new cusktomer id
	 */
	public void setCusktomerID(Long cusktomerID) {
		this.customerID = cusktomerID;
	}

}




