/*
 * 
 */
package com.mettl.abc.domains;

import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class Transaction.
 *
 * @author Akash Mittal
 * @@task Loyality Program Assignment
 */
public class Transaction{
	
	/** The transaction id. */
	private Long transactionID;
	
	/** The purchase date. */
	private Date purchaseDate;
	
	/** The purchase amount. */
	private Double purchaseAmount;
	
	/**
	 * Gets the transaction id.
	 *
	 * @return the transaction id
	 */
	public Long getTransactionID() {
		return transactionID;
	}
	
	/**
	 * Sets the transaction id.
	 *
	 * @param transactionID the new transaction id
	 */
	public void setTransactionID(Long transactionID) {
		this.transactionID = transactionID;
	}
	
	/**
	 * Gets the purchase date.
	 *
	 * @return the purchase date
	 */
	public Date getPurchaseDate() {
		return purchaseDate;
	}
	
	/**
	 * Sets the purchase date.
	 *
	 * @param purchaseDate the new purchase date
	 */
	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
	/**
	 * Gets the purchase amount.
	 *
	 * @return the purchase amount
	 */
	public Double getPurchaseAmount() {
		return purchaseAmount;
	}
	
	/**
	 * Sets the purchase amount.
	 *
	 * @param purchaseAmount the new purchase amount
	 */
	public void setPurchaseAmount(Double purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Transaction [TransactionID=" + transactionID
				+ ", purchaseDate=" + purchaseDate + ", purchaseAmount="
				+ purchaseAmount + "]";
	}
	
	
}
