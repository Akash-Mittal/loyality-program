/*
 * 
 */
package com.mettl.abc.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import org.apache.log4j.Logger;

import com.mettl.abc.domains.Customer;
import com.mettl.abc.domains.EODTransactions;
import com.mettl.abc.service.CustomerLoyalityServiceImpl;
import com.mettl.abc.utils.FileUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class MainClient.
 *
 * @author Akash Mittal
 * @@task Loyality Program Assignment
 */
public class MainClient {

	/** The logger. */
	private static Logger logger = Logger.getLogger(MainClient.class);

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)  {
		try{
		
		CustomerLoyalityServiceImpl customerLoyalityService;
		ArrayList<Customer> existingCustomers ;
		ArrayList<EODTransactions> eodTransactions;
		final String pathtoPropertiesFile;
		if (Objects.nonNull(args)) {
			if (args.length>0) {
				logger.debug("Initiating from CLI ...");
				 pathtoPropertiesFile = args[0];
				 try{
				customerLoyalityService = new CustomerLoyalityServiceImpl(
						FileUtils.loadProperties(pathtoPropertiesFile));
				logger.debug("Loaded succesfully from "+pathtoPropertiesFile);
				 }catch(IOException exception){
						logger.debug("Coudnot locate the file looking in jar");
					 customerLoyalityService = new CustomerLoyalityServiceImpl(
								FileUtils.loadPropertiesResource(pathtoPropertiesFile));
						logger.debug("Loaded succesfully from "+Thread.currentThread().getClass().getClassLoader().getResource(pathtoPropertiesFile).getPath());
					 
				 }
				 catch(NullPointerException nullPointerException){
						logger.debug("Coudnot locate the file looking in given path looking for default");
					 customerLoyalityService = new CustomerLoyalityServiceImpl(
								FileUtils.loadPropertiesResource("/config.properties"));
						logger.debug("Loaded succesfully from "+Thread.currentThread().getClass().getClassLoader().getResource(pathtoPropertiesFile).getPath());
						logger.debug("All the default resources files canbe found in the jar" );

				 }
				if (!pathtoPropertiesFile.isEmpty()) {
					logger.debug("Reading PropertyFile from - "
							+ pathtoPropertiesFile);
					logger.debug("Getting existing data+");
					 existingCustomers = customerLoyalityService
							.getExistingCustomer();
					logger.debug("Getting EOD Transactio data+");
					 eodTransactions = customerLoyalityService
							.getEODTransactionsData();
					logger.debug("Computing Class and Reward Points");
					logger.debug("Genereating XML report ");
					customerLoyalityService.printReportForEachCustomer(
							customerLoyalityService
							.addTransactionDetailsToCustomer(existingCustomers,
									eodTransactions), "XML");
					
				} else{
					logger.debug("Please provide correct Path to - Not a correct format");
					//executeInDefaultMode = true;
				}	
			}else{ 
				logger.debug("Please provide correct Path ");
				//executeInDefaultMode = true;
			}
		} else {
			logger.debug("Arguments Missing...");
			//executeInDefaultMode = true;
		}

		}catch(IOException exception){logger.debug("example java -jar target\\mainfile.jar complete.path.config.properties");
		logger.debug("Please enter the correct Path");
		logger.debug("example java -jar target\\mainfile.jar complete.path.config.properties");
		}catch(Exception e ){
			logger.debug(e);
		}
	}


}