/*
 * 
 */
package com.mettl.abc.utils;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;

import com.mettl.abc.domains.Customer;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.collections.MapConverter;
import com.thoughtworks.xstream.io.xml.DomDriver;

// TODO: Auto-generated Javadoc
/**
 * Provides utility methods to read and write arbitrary Java objects as xml
 * (xml-Serialization using the xstream library).
 * 
 *
 */
public class XMLUtil {

	/**
	 * Loads an object from a String containing xml.
	 *
	 * @param xmlString            A String containing xml data.
	 * @return the object
	 * @see #saveXML(Object)
	 */
	public static Object loadXML(String xmlString) {
		XStream xstream = getXStream();
		Object o = xstream.fromXML(xmlString);
		return o;
	}

	/**
	 * Loads an object from a file.
	 *
	 * @param f            The file to load.
	 * @return the object
	 */
	public static Object loadXML(File f) {
		XStream xstream = getXStream();
		Object o = xstream.fromXML(f);
		return o;
	}

	/**
	 * Loads an object from a {@link Reader}.
	 *
	 * @param r            Source of the xml.
	 * @return the object
	 */
	public static Object loadXML(Reader r) {
		XStream xstream = getXStream();
		Object o = xstream.fromXML(r);
		return o;
	}

	/**
	 * Converts an object into a xml String.
	 * 
	 * @param o
	 *            The object to convert.
	 * @return The object serialized to xml.
	 */
	public static String saveXML(Object o) {
		XStream xstream = getXStream();
		return xstream.toXML(o);
	}

	/**
	 * Converts an object into xml and writes the result in {@code w}.
	 * 
	 * @param o
	 *            The object to convert.
	 * @param w
	 *            The output writer.
	 */
	public static void saveXML(Object o, Writer w) {
		XStream xstream = getXStream();
		xstream.toXML(o, w);
	}

	/**
	 * Converts an object into xml and saves the result in a file {@code f}.
	 * 
	 * @param o
	 *            The object to convert.
	 * @param f
	 *            The output file. This file is overwritten if it already
	 *            exists.
	 */
	public static void saveXML(Object o, File f) {
		XStream xstream = getXStream();
		try {
			BufferedWriter fw = new BufferedWriter(new FileWriter(f));
			xstream.toXML(o, fw);
			fw.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Gets the x stream.
	 *
	 * @return the x stream
	 */
	private static XStream getXStream() {
		XStream xstream = new XStream(new DomDriver());
		return xstream;
	}
	
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		File file = new File("D:\\Java\\Practise\\ABC Limited\\loyality-program\\src\\main\\resources\\Customer.xml");
		//System.out.println(
		//file.getAbsolutePath());
		ArrayList<Customer> customer = new ArrayList<Customer>();
		customer.add(DefaultData.getCustomerDefaultData());
		customer.add(DefaultData.getCustomerDefaultData());
		System.out.println(XMLUtil.saveXML(customer));
		
//		Customer customer = (Customer)XMLUtil.loadXML(file);
	//	System.out.println(customer.toString());
		//customer.toString();
	}
}
