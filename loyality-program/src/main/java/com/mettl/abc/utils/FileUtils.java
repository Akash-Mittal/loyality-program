/*
 * 
 */

package com.mettl.abc.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Properties;


// TODO: Auto-generated Javadoc
/**
 * The Class FileUtils.
 *
 * @author akash Mittal
 */
public class FileUtils {
    
    /**
     * Instantiates a new file utils.
     */
    private FileUtils() {}

    /**
     * File exists.
     *
     * @param path the path
     * @return true, if successful
     */
    public static boolean fileExists(String path) {
        File file = new File(path);
        return file.exists();
    }
    
    /**
     * Write file.
     *
     * @param path the path
     * @param text the text
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void writeFile(String path, String text) throws IOException {
        writeFile(new File(path), text, false);
    }

    /**
     * Write file.
     *
     * @param path the path
     * @param text the text
     * @param append the append
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void writeFile(String path, String text, boolean append) throws IOException {
        writeFile(new File(path), text, append);
    }

    /**
     * Write file.
     *
     * @param file the file
     * @param text the text
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void writeFile(File file, String text) throws IOException {
        writeFile(file, text, false);
    }

    /**
     * Write file.
     *
     * @param file the file
     * @param text the text
     * @param append the append
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void writeFile(File file, String text, boolean append) throws IOException {        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new FileWriter(file, append), true);
            writer.println(text);
        }
        finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    /**
     * Load file.
     *
     * @param file the file
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static String loadFile(File file) throws IOException {
        return loadFile(file, true);
    }

    /**
     * Load file.
     *
     * @param path the path
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static String loadFile(String path) throws IOException {
        return loadFile(new File(path), true);
    }

    /**
     * Load file.
     *
     * @param path the path
     * @param escapeLines the escape lines
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static String loadFile(String path, boolean escapeLines) throws IOException {
        return loadFile(new File(path), escapeLines);
    }

    /**
     * Load file.
     *
     * @param file the file
     * @param escapeLines the escape lines
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static String loadFile(File file, boolean escapeLines) throws IOException {
        FileReader fileReader = null;
        BufferedReader reader = null;

        try {
            fileReader = new FileReader(file);
            reader = new BufferedReader(fileReader);

            String value = null;
            StringBuffer sb = new StringBuffer();

            while ((value = reader.readLine()) != null) {
                sb.append(value);

                if (escapeLines) {
                    sb.append('\n');
                }

            }

            return sb.toString();
        }
        finally {
            if (reader != null) {
                reader.close();
            }
            if (fileReader != null) {
                fileReader.close();
            }
        }
    }

    /**
     * Load resource.
     *
     * @param path the path
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static String loadResource(String path) throws IOException {
        InputStream input = null;

        try {
            ClassLoader cl = FileUtils.class.getClassLoader();
            
            if (cl != null) {
                input = cl.getResourceAsStream(path);
            }
            else {
                input = ClassLoader.getSystemResourceAsStream(path);
            }

            int i = 0;
            StringBuffer buf = new StringBuffer();
            
            while ((i = input.read()) != -1) {
                buf.append((char)i);
            }

            return buf.toString();
        }
        finally {
            if (input != null) {
                input.close();
            }
        }
    }

    /**
     * Load properties.
     *
     * @param path the path
     * @param defaults the defaults
     * @return the properties
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Properties loadProperties(String path, Properties defaults) throws IOException {
        return loadProperties(new File(path), defaults);
    }

    /**
     * Load properties.
     *
     * @param file the file
     * @param defaults the defaults
     * @return the properties
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Properties loadProperties(File file, Properties defaults) throws IOException {
        InputStream input = null;

        try {
            Properties properties = null;

            if (defaults != null) {
                properties = new Properties(defaults);
            }
            else {
                properties = new Properties();
            }

            input = new FileInputStream(file);
            properties.load(input);
            return properties;
        }
        finally {
            if (input != null) {
                input.close();
            }   
        }

    }

    /**
     * Load properties.
     *
     * @param path the path
     * @return the properties
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Properties loadProperties(String path) throws IOException {
        return loadProperties(new File(path), null);
    }

    /**
     * Load properties.
     *
     * @param file the file
     * @return the properties
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Properties loadProperties(File file) throws IOException {
        return loadProperties(file, null);
    }

    /**
     * Store properties.
     *
     * @param path the path
     * @param properties the properties
     * @param header the header
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void storeProperties(String path, 
            Properties properties, String header) throws IOException {
        storeProperties(new File(path), properties, header);
    }

    /**
     * Store properties.
     *
     * @param file the file
     * @param properties the properties
     * @param header the header
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void storeProperties(File file, 
            Properties properties, String header) throws IOException {
        FileOutputStream output = null;        
        try {
            output = new FileOutputStream(file);
            properties.store(output, header);
        }
        finally {
            if (output != null) {
                output.close();
            }
        }
    }
    
    /**
     * Load properties resource.
     *
     * @param path the path
     * @return the properties
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Properties loadPropertiesResource(String path) throws IOException {
        InputStream input = null;
        
        try {
            ClassLoader cl = FileUtils.class.getClassLoader();            

            if (cl != null) {
                input = cl.getResourceAsStream(path);
            }
            else {
                input = ClassLoader.getSystemResourceAsStream(path);
            }

            Properties properties = new Properties();
            properties.load(input);
            input.close();
            return properties;
        }
        finally {
            if (input != null) {
                input.close();
            }
        }
    }

    /**
     * Read object.
     *
     * @param path the path
     * @return the object
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Object readObject(String path) throws IOException {
        return readObject(new File(path));
    }

    /**
     * Read object.
     *
     * @param file the file
     * @return the object
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Object readObject(File file) throws IOException {
        FileInputStream fileIn = null;
        BufferedInputStream buffIn = null;
        ObjectInputStream obIn = null;

        try {
            fileIn = new FileInputStream(file);
            buffIn = new BufferedInputStream(fileIn);
            obIn = new ObjectInputStream(buffIn);
            return obIn.readObject();
        }
        catch (ClassNotFoundException cExc) {
            cExc.printStackTrace();
            return null;
        }
        finally {
            try {
                if (obIn != null) {
                    obIn.close();
                }
                if (buffIn != null) {
                    buffIn.close();
                }
                if (fileIn != null) {
                    fileIn.close();
                }
            } catch (IOException e) {}
        }
    }
    
    /**
     * Write object.
     *
     * @param object the object
     * @param path the path
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void writeObject(Object object, String path) throws IOException {
        FileOutputStream fileOut = null;
        BufferedOutputStream bufferedOut = null;
        ObjectOutputStream obOut = null;

        try {
            fileOut = new FileOutputStream(path);
            bufferedOut = new BufferedOutputStream(fileOut);
            obOut = new ObjectOutputStream(bufferedOut);
            obOut.writeObject(object);
        }
        finally {
            try {
                if (bufferedOut != null) {
                    bufferedOut.close();
                }
                if (obOut != null) {
                    obOut.close();
                }
                if (fileOut != null) {
                    fileOut.close();
                }
            } catch (IOException e) {}
        }

    }

    /** The default buffer size. */
    private static int defaultBufferSize = 32768;

    /**
     * Copy resource.
     *
     * @param from the from
     * @param to the to
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void copyResource(String from, String to) throws IOException {
        InputStream in = null;
        OutputStream out = null;
        
        try {
            ClassLoader cl = FileUtils.class.getClassLoader();
            
            if (cl != null) {
                in = cl.getResourceAsStream(from);
            }
            else {
                in = ClassLoader.getSystemResourceAsStream(from);
            }

            out = new FileOutputStream(to);

            byte[] buffer = new byte[defaultBufferSize];
            while (true) {
                synchronized(buffer) {
                    int amountRead = in.read(buffer);
                    if (amountRead == -1) {
                        break;
                    }
                    out.write(buffer, 0, amountRead);
                }
            }

        }
        finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }            
        }
    }

    /**
     * Copy file.
     *
     * @param from the from
     * @param to the to
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void copyFile(String from, String to) throws IOException {
        InputStream in = null;
        OutputStream out = null;
        
        try {
            in = new FileInputStream(from);
            out = new FileOutputStream(to);

            byte[] buffer = new byte[defaultBufferSize];
            while (true) {
                synchronized(buffer) {
                    int amountRead = in.read(buffer);
                    if (amountRead == -1) {
                        break;
                    }
                    out.write(buffer, 0, amountRead);
                }
            }

        }
        finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }            
        }
    }




/**
 * The main method.
 *
 * @param args the arguments
 * @throws Exception the exception
 */
public static void main(String args[]) throws Exception{
    writeFile("summ.sql", loadFile("summ.sql").replaceAll("\n", ""));

    
    
    if(args.length==3){
    //    COMMAND INPUTFILE  TEXTLINE                 
       if(args[0].equals("RemoveLinesWithSpecificText")){ 
                        writeFile(args[1],loadFile(args[1]).replaceAll(args[2],""));
    }
    }  


        if(args.length==2){
            //    COMMAND INPUTFILE  
            if(args[0].equals("RemoveBlankLines")){
                
                writeFile(args[1], loadFile(args[1]).replaceAll("\n\n\n\n", ""));
            }
            }


    if(args.length==2){
            //    COMMAND INPUTFILE  
            if(args[0].equals("RemoveBlankLinesModiFied")){
                writeFile(args[1], loadFile(args[1]).replaceAll("\n", ""));
            }
            }


}

    
}
