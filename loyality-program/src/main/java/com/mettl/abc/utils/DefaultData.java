/*
 * 
 */
package com.mettl.abc.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import com.mettl.abc.domains.Customer;
import com.mettl.abc.domains.EODTransactions;
import com.mettl.abc.domains.Transaction;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultData. This is for Test cases Mocking
 *
 * @author Akash Mittal
 * @@task Loyality Program Assignment
 */
public class DefaultData {

	
	
	/**
	 * Gets the customer default data.
	 *
	 * @return the customer default data
	 */
	public static Customer getCustomerDefaultData(){
		Transaction transaction1 = new Transaction();
		transaction1.setTransactionID(123L);
		transaction1.setPurchaseAmount(112323.12);
		transaction1.setPurchaseDate(Calendar.getInstance().getTime());
		Transaction transaction2 = new Transaction();
		transaction2.setTransactionID(124L);
		transaction2.setPurchaseAmount(1123.12);
		transaction2.setPurchaseDate(Calendar.getInstance().getTime());
		Customer customer = new Customer();
		customer.setCustomerID(2323l);
		customer.setCustomerID(24522L);
		customer.setCustomerName("Ram Ram g");
		ArrayList<Transaction> customerTransactions = new ArrayList<Transaction>();
		customerTransactions.add(transaction1);
		customerTransactions.add(transaction2);
		customer.setCustomerTransactions(customerTransactions);
		customer.setEmail("yo.gmail.com");
		
		return customer;
	}
		
	
	/**
	 * Gets the EOD data.
	 *
	 * @return the EOD data
	 */
	public  static EODTransactions getEODData(){
		EODTransactions eodTransactions = new EODTransactions();
			eodTransactions.setCustomerName("Abhay");
			eodTransactions.setEmail("abhay@demo.com");
			eodTransactions.setCustomerLoyalityCardNumber(11001L);
			eodTransactions.setPurchaseAmount(7402.00);
			eodTransactions.setPurchaseDate(Calendar.getInstance().getTime());
			eodTransactions.setTransactionID(2348723L);
			return eodTransactions;
	}

	
}
